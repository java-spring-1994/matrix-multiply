import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

public class MatrixMultiplicationWithThread {

	int m1[][], m2[][], resultMatrix[][], n;

	public static Callable<Void> addtask(int[][] m1, int[][] m2, int[][] resultMatrix, int i, int j, int n){
		return new Callable<Void>() {
			public Void call() throws Exception {
				MultiplierHelper.multiply(m1, m2, resultMatrix, i, j, n);
				//return "mul " + i + " " + j;
				return null;
			}
		};
	}

	public int[][] calculate(int[][] m1, int[][] m2) throws InterruptedException, ExecutionException {
		this.m1 = m1;
		this.m2 = m2;
		this.n = m1.length;
		resultMatrix = new int[n][n];

		ExecutorService executorService = Executors.newFixedThreadPool(10);

		Set<Callable<Void>> callables = new HashSet<Callable<Void>>();

		for (int i = 0; i< n; i++) {
			for (int j = 0; j < n ; j++) {
				callables.add(addtask(m1, m2, resultMatrix, i, j, n));
			}
		}

		executorService.invokeAll(callables);
		executorService.shutdown();
		return resultMatrix;
	}

}
